package pojo;

public class Events {
	private String eventId;
	private String subEventId;
	private String recurrenceId;
	private String correlationId;
	private String seriesId;
	private String eventName;
	private String description;
	private String startDateTime;
	private String endDateTime;
	private String captureDate;
	private String category;
	private String subCategory;
	private String eventStatus;
	private int estimatedAttendees;
	private int capacity;
	private String attendanceClass;
	private String sourceId;
	private Location location;

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getEventId() {
		return this.eventId;
	}

	public void setSubEventId(String subEventId) {
		this.subEventId = subEventId;
	}

	public String getSubEventId() {
		return this.subEventId;
	}

	public void setRecurrenceId(String recurrenceId) {
		this.recurrenceId = recurrenceId;
	}

	public String getRecurrenceId() {
		return this.recurrenceId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getCorrelationId() {
		return this.correlationId;
	}

	public void setSeriesId(String seriesId) {
		this.seriesId = seriesId;
	}

	public String getSeriesId() {
		return this.seriesId;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventName() {
		return this.eventName;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return this.description;
	}

	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	public String getStartDateTime() {
		return this.startDateTime;
	}

	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getEndDateTime() {
		return this.endDateTime;
	}

	public void setCaptureDate(String captureDate) {
		this.captureDate = captureDate;
	}

	public String getCaptureDate() {
		return this.captureDate;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return this.category;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getSubCategory() {
		return this.subCategory;
	}

	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}

	public String getEventStatus() {
		return this.eventStatus;
	}

	public void setEstimatedAttendees(int estimatedAttendees) {
		this.estimatedAttendees = estimatedAttendees;
	}

	public int getEstimatedAttendees() {
		return this.estimatedAttendees;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getCapacity() {
		return this.capacity;
	}

	public void setAttendanceClass(String attendanceClass) {
		this.attendanceClass = attendanceClass;
	}

	public String getAttendanceClass() {
		return this.attendanceClass;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceId() {
		return this.sourceId;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Location getLocation() {
		return this.location;
	}
}
