package pojo;

import java.util.List;

public class addSpecialEvent {
    private List<Events> events;
    private Metadata metadata;

    public void setEvents(List<Events> events){
        this.events = events;
    }
    public List<Events> getEvents(){
        return this.events;
    }
    public void setMetadata(Metadata metadata){
        this.metadata = metadata;
    }
    public Metadata getMetadata(){
        return this.metadata;
    }
}
