package pojo;

import java.util.ArrayList;
import java.util.List;
public class PropertyDemandIndicator
{
    private String propertyId;

    private String ideasPropertyId;

    private String propertyName;

    private ProximityRange proximityRange;

    private List<Scores> scores;

    private String vendor;

    private String sessionId;

    private String innCode;

    public void setPropertyId(String propertyId){
        this.propertyId = propertyId;
    }
    public String getPropertyId(){
        return this.propertyId;
    }
    public void setIdeasPropertyId(String ideasPropertyId){
        this.ideasPropertyId = ideasPropertyId;
    }
    public String getIdeasPropertyId(){
        return this.ideasPropertyId;
    }
    public void setPropertyName(String propertyName){
        this.propertyName = propertyName;
    }
    public String getPropertyName(){
        return this.propertyName;
    }
    public void setProximityRange(ProximityRange proximityRange){
        this.proximityRange = proximityRange;
    }
    public ProximityRange getProximityRange(){
        return this.proximityRange;
    }
    public void setScores(List<Scores> scores){
        this.scores = scores;
    }
    public List<Scores> getScores(){
        return this.scores;
    }
    public void setVendor(String vendor){
        this.vendor = vendor;
    }
    public String getVendor(){
        return this.vendor;
    }
    public void setSessionId(String sessionId){
        this.sessionId = sessionId;
    }
    public String getSessionId(){
        return this.sessionId;
    }
    public void setInnCode(String innCode){
        this.innCode = innCode;
    }
    public String getInnCode(){
        return this.innCode;
    }
}