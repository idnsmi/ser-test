package pojo;

public class Scores
{
    private double demandScore;

    private String forDate;

    public void setDemandScore(double demandScore){
        this.demandScore = demandScore;
    }
    public double getDemandScore(){
        return this.demandScore;
    }
    public void setForDate(String forDate){
        this.forDate = forDate;
    }
    public String getForDate(){
        return this.forDate;
    }
}