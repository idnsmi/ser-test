package pojo;

public class Metadata {
    private String sessionId;
    private int pageSize;
    private int currentPage;
    private int numberOfPages;
    private int totalElements;

    public void setSessionId(String sessionId){
        this.sessionId = sessionId;
    }
    public String getSessionId(){
        return this.sessionId;
    }
    public void setPageSize(int pageSize){
        this.pageSize = pageSize;
    }
    public int getPageSize(){
        return this.pageSize;
    }
    public void setCurrentPage(int currentPage){
        this.currentPage = currentPage;
    }
    public int getCurrentPage(){
        return this.currentPage;
    }
    public void setNumberOfPages(int numberOfPages){
        this.numberOfPages = numberOfPages;
    }
    public int getNumberOfPages(){
        return this.numberOfPages;
    }
    public void setTotalElements(int totalElements){
        this.totalElements = totalElements;
    }
    public int getTotalElements(){
        return this.totalElements;
    }
}
