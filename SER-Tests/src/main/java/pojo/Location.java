package pojo;

public class Location {
    private String city;
    private String countrySubdivsion;
    private String country;
    private String region;
    private Venue venue;

    public void setCity(String city){
        this.city = city;
    }
    public String getCity(){
        return this.city;
    }
    public void setCountrySubdivsion(String countrySubdivsion){
        this.countrySubdivsion = countrySubdivsion;
    }
    public String getCountrySubdivsion(){
        return this.countrySubdivsion;
    }
    public void setCountry(String country){
        this.country = country;
    }
    public String getCountry(){
        return this.country;
    }
    public void setRegion(String region){
        this.region = region;
    }
    public String getRegion(){
        return this.region;
    }
    public void setVenue(Venue venue){
        this.venue = venue;
    }
    public Venue getVenue(){
        return this.venue;
    }
}
