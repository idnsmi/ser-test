package pojo;

public class Venue {
    private String venueId;
    private String venueName;
    private String address;
    private String address2;
    private String postalCode;
    private double latitude;
    private double longitude;

    public void setVenueId(String venueId){
        this.venueId = venueId;
    }
    public String getVenueId(){
        return this.venueId;
    }
    public void setVenueName(String venueName){
        this.venueName = venueName;
    }
    public String getVenueName(){
        return this.venueName;
    }
    public void setAddress(String address){
        this.address = address;
    }
    public String getAddress(){
        return this.address;
    }
    public void setAddress2(String address2){
        this.address2 = address2;
    }
    public String getAddress2(){
        return this.address2;
    }
    public void setPostalCode(String postalCode){
        this.postalCode = postalCode;
    }
    public String getPostalCode(){
        return this.postalCode;
    }
    public void setLatitude(double latitude){
        this.latitude = latitude;
    }
    public double getLatitude(){
        return this.latitude;
    }
    public void setLongitude(double longitude){
        this.longitude = longitude;
    }
    public double getLongitude(){
        return this.longitude;
    }
}
