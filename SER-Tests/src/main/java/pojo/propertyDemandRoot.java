package pojo;

public class propertyDemandRoot
{
    private PropertyDemandIndicator propertyDemandIndicator;

    private int count;

    public void setPropertyDemandIndicator(PropertyDemandIndicator propertyDemandIndicator){
        this.propertyDemandIndicator = propertyDemandIndicator;
    }
    public PropertyDemandIndicator getPropertyDemandIndicator(){
        return this.propertyDemandIndicator;
    }
    public void setCount(int count){
        this.count = count;
    }
    public int getCount(){
        return this.count;
    }
}