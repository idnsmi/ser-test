Feature: Validating Special Events API

@AddSpecialEvent
Scenario: Verify if SpecialEvent is being successfully added using SpecialEvents API
	Given Add SER payload with valid token
	When system calls SpecialEvents API with PUT http request
	Then the API call got success with status code 200
	And "Message processed successfully" in response body

@AddSpecialEventWithInvalidToken
Scenario: Verify if SpecialEvent does not get added if Token is invalid
	Given Add SER payload with invalid token
	When system calls SpecialEvents API with PUT http request
	Then the API call got success with status code 401
	And Failure "Unauthorized" text in response body
	
@AddSpecialEventWithInvalidFields	
Scenario: Verify if SpecialEvent does not get added if request has invalid fields
	Given Add SER payload with invalid fields
	When system calls SpecialEvents API with PUT http request
	Then the API call got success with status code 400
	And Failure text in response body

@AddSpecialEventWithoutCompulsoryFields	
Scenario: Verify if SpecialEvent does not get added if request does not have compulsory fields
	Given Add SER payload without compulsory fields
	When system calls SpecialEvents API with PUT http request
	Then the API call got success with status code 400
	And Failure text of compulsory fields in response body