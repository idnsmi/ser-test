Feature: Validating Property Demands API

@AddPropertyDemands	
Scenario: Verify if Property Demand get added using PropertyDemands API
	Given Add Property Demand payload  with valid token
	When system calls PropertyDemands API with PUT http request
	Then the API call got success with status code 200
	And "Message processed successfully" in response body
	And Check if the Property Demand has got added
	
@AddPropertyDemandstWithInvalidToken
Scenario: Verify if Property Demand does not get added if Token is invalid
	Given Add Property Demand payload  with invalid token
	When system calls PropertyDemands API with PUT http request
	Then the API call got success with status code 401
	And Failure "Unauthorized" text in response body
	
@AddPropertyDemandstWithInvalidFields
Scenario: Verify if Property Demand does not get added if fields are invalid
	Given Add Property Demand payload  with invalid fields
	When system calls PropertyDemands API with PUT http request
	Then the API call got success with status code 400
	And Failure text for Property Demand in response body
	
@AddPropertyDemandstWithoutCompulsoryFields
Scenario: Verify if Property Demand does not get added if request does not have compulsory fields
	Given Add Property Demand payload  without compulsory fields
	When system calls PropertyDemands API with PUT http request
	Then the API call got success with status code 400
	And "Some of the required data fields are missing" in response body
	