package resources;

import static io.restassured.RestAssured.given;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Properties;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class utils {
	
	public static RequestSpecification req;
	public static String resultoken;
	public RequestSpecification requestSpecification() throws IOException {
		
		RestAssured.baseURI = "https://fds.stage.ideasrms.com";
		String Bearertoken = "MzlnaHEzb29tMDBxYW5qZjllMWQ0MDRtbXY6Z2twbmo2aTZzdWE5dTFlOTQ2cWU0amI4amJtN2VqdXM5Z25lbTZrYjc5dHFwNWRpNzAy";
				
	
		String token = given()//.log().all()
		.header("Authorization", "Basic "+Bearertoken)
		.when().get("/api/ups/ideas/v1/token/serTestVendor")
		.then()//.log().all()
		.assertThat().statusCode(200)
		.extract().response().asString();
		
		resultoken = token.substring(1, token.length() - 1);
		//System.out.println("The token is "+resultoken);

			req = new RequestSpecBuilder().setBaseUri("https://ser-lambda.dev.ideasrms.com/").addHeader("Authorization", "Bearer "+resultoken)
					.addHeader("Content-Type","application/json").setContentType(ContentType.JSON).build();

			return req;
	}
	
	public RequestSpecification invalidRequestSpecification() throws IOException {
		
		RestAssured.baseURI = "https://fds.stage.ideasrms.com";
		String Bearertoken = "null";
				
	
		String token = given()//.log().all()
		.header("Authorization", "Basic "+Bearertoken)
		.when().get("/api/ups/ideas/v1/token/serTestVendor")
		.then()//.log().all()
		.assertThat().statusCode(200)
		.extract().response().asString();
		
		resultoken = token.substring(1, token.length() - 1);
		//System.out.println("The token is "+resultoken);

			req = new RequestSpecBuilder().setBaseUri("https://ser-lambda.dev.ideasrms.com/").addHeader("Authorization", "Bearer "+resultoken)
					.addHeader("Content-Type","application/json").setContentType(ContentType.JSON).build();

			return req;
	}
	
	public RequestSpecification requestSpecificationPropertyDemand() throws IOException {
		
		RestAssured.baseURI = "https://fds.stage.ideasrms.com";
		String Bearertoken = "MzlnaHEzb29tMDBxYW5qZjllMWQ0MDRtbXY6Z2twbmo2aTZzdWE5dTFlOTQ2cWU0amI4amJtN2VqdXM5Z25lbTZrYjc5dHFwNWRpNzAy";
				
	
		String token = given()//.log().all()
		.header("Authorization", "Basic "+Bearertoken)
		.when().get("/api/ups/ideas/v1/token/serTestVendor")
		.then()//.log().all()
		.assertThat().statusCode(200)
		.extract().response().asString();
		
		resultoken = token.substring(1, token.length() - 1);
		//System.out.println("The token is "+resultoken);

			req = new RequestSpecBuilder().setBaseUri("https://q2396wzrj6.execute-api.us-east-2.amazonaws.com").addHeader("Authorization", "Bearer "+resultoken)
					.addHeader("Content-Type","application/json").setContentType(ContentType.JSON).build();

			return req;
	}
	
public RequestSpecification invalidrequestSpecificationPropertyDemand() throws IOException {
		
		RestAssured.baseURI = "https://fds.stage.ideasrms.com";
		String Bearertoken = "null";
				
	
		String token = given()//.log().all()
		.header("Authorization", "Basic "+Bearertoken)
		.when().get("/api/ups/ideas/v1/token/serTestVendor")
		.then()//.log().all()
		.assertThat().statusCode(200)
		.extract().response().asString();
		
		resultoken = token.substring(1, token.length() - 1);
		//System.out.println("The token is "+resultoken);

			req = new RequestSpecBuilder().setBaseUri("https://q2396wzrj6.execute-api.us-east-2.amazonaws.com").addHeader("Authorization", "Bearer "+resultoken)
					.addHeader("Content-Type","application/json").setContentType(ContentType.JSON).build();

			return req;
	}

}
