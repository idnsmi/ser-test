package resources;

import java.util.Random;

public class TestDataBuilder {

	public static String addSERPayload() {
		
		String randomNumber = getRandomNumberString();
		
		return "{\"events\":[{\"eventId\":\"Event-"+randomNumber+"\",\"subEventId\":\"SubEvent-"+randomNumber+"\",\"recurrenceId\":\"Recc-"+randomNumber+"\",\"correlationId\":\"Corr-"+randomNumber+"\",\"seriesId\":\"Series-"+randomNumber+"\",\"eventName\":\"Event-"+randomNumber+"\",\"description\":\"Description of Event "+randomNumber+"\",\"startDateTime\":\"2021-10-20T10:00:00Z\",\"endDateTime\":\"2021-10-21T10:00:00Z\",\"captureDate\":\"2021-10-18T10:00:00Z\",\"category\":\"SPORTS\",\"subCategory\":\"NBA\",\"eventStatus\":\"VALID\",\"estimatedAttendees\":5,\"capacity\":600,\"attendanceClass\":\"D\",\"sourceId\":\"3\",\"location\":{\"city\":\"San Francisco\",\"countrySubdivsion\":\"CA\",\"country\":\"CHILE\",\"region\":\"NORTH_AMERICA\",\"venue\":{\"venueId\":\"33e95d8e-b72a-46ce-aa2b-b242ef9995999001\",\"venueName\":\"The Convention Center\",\"address\":\"123 Road Ln\",\"address2\":\"Suite 1000\",\"postalCode\":\"94102\",\"latitude\":18.5204,\"longitude\":73.8567}}}],\"metadata\":{\"sessionId\":\"33e95d8e-b72a-46ce-aa2b-b242ef9995"+randomNumber+"\",\"pageSize\":1,\"currentPage\":1,\"numberOfPages\":1,\"totalElements\":1}}";

}
	
public static String addInvalidSERPayload() {
		
		//String randomNumber = getRandomNumberString();
		
		return "{\"events\":[{\"eventId\":1,\"subEventId\":1,\"recurrenceId\":1,\"correlationId\":1,\"seriesId\":1,\"eventName\":1,\"description\":1,\"startDateTime\":1,\"endDateTime\":1,\"captureDate\":1,\"category\":1,\"subCategory\":1,\"eventStatus\":1,\"estimatedAttendees\":\"ABC\",\"capacity\":\"ABC\",\"attendanceClass\":1,\"sourceId\":1,\"location\":{\"city\":1,\"countrySubdivsion\":1,\"country\":1,\"region\":1,\"venue\":{\"venueId\":1,\"venueName\":1,\"address\":1,\"address2\":1,\"postalCode\":1,\"latitude\":\"ABC\",\"longitude\":\"ABC\"}}}],\"metadata\":{\"sessionId\":1,\"pageSize\":\"ABC\",\"currentPage\":\"ABC\",\"numberOfPages\":\"ABC\",\"totalElements\":\"ABC\"}}";

}

public static String addNonCompulsoryFieldSERPayload() {
	
	//String randomNumber = getRandomNumberString();
	
	return "{\"events\":[{\"recurrenceId\":\"Recc-000022\",\"correlationId\":\"Corr-000022\",\"seriesId\":\"Series-000022\",\"description\":\"Description of Event 000022\",\"captureDate\":\"2021-10-18T10:00:00Z\",\"eventStatus\":\"VALID\",\"estimatedAttendees\":5,\"capacity\":600,\"attendanceClass\":\"D\",\"sourceId\":\"3\",\"location\":{\"city\":\"San Francisco\",\"countrySubdivsion\":\"CA\",\"venue\":{\"venueId\":\"33e95d8e-b72a-46ce-aa2b-b242ef9995999001\",\"venueName\":\"The Convention Center\",\"address\":\"123 Road Ln\",\"address2\":\"Suite 1000\",\"postalCode\":\"94102\",\"latitude\":18.5204,\"longitude\":73.8567}}}],\"metadata\":{}}";

}

public static String addPropertyDemandPayload() {
	
	String randomNumber = getRandomNumberString();
	
	return "{\"propertyDemandIndicators\":[{\"propertyId\":\"3101759\",\"ideasPropertyId\":\"5db419a2-85f7-4a3c-9ca5-1e09a62dbdd5\",\"propertyName\":\"Bahia Mar Ft. Lauderdale Beach- a DoubleTree by Hilton Hotel\",\"proximityRange\":{\"number\":1,\"unit\":\"meters\"},\"scores\":[{\"demandScore\":59.6,\"forDate\":\"2022-01-06\"}]}],\"metadata\":{\"sessionId\":\"33e95d8e-b72a-46ce-aa2b-b242ef"+randomNumber+"\",\"pageSize\":1,\"currentPage\":1,\"numberOfPages\":1,\"totalElements\":1}}";

}

public static String addInvalidPropertyDemandPayload() {
	
	//String randomNumber = getRandomNumberString();
	
	return "{\"propertyDemandIndicators\":[{\"propertyId\":1,\"ideasPropertyId\":1,\"propertyName\":1,\"proximityRange\":{\"number\":\"text\",\"unit\":1},\"scores\":[{\"demandScore\":\"Text\",\"forDate\":1}]}],\"metadata\":{\"sessionId\":1,\"pageSize\":\"Text\",\"currentPage\":\"Text\",\"numberOfPages\":\"Text\",\"totalElements\":\"Text\"}}";

}

public static String addNonCompulsoryPropertyDemandPayload() {
	
	String randomNumber = getRandomNumberString();
	
	return "{\"propertyDemandIndicators\":[],\"metadata\":{\"sessionId\":\"33e95d8e-b72a-46ce-aa2b-b242ef"+randomNumber+"\",\"pageSize\":1,\"currentPage\":1,\"numberOfPages\":1,\"totalElements\":1}}";

}
	
	public static String getRandomNumberString() {
	    // It will generate 6 digit random Number.
	    // from 0 to 999999
	    Random rnd = new Random();
	    int number = rnd.nextInt(999999);

	    // this will convert any number sequence into 6 character.
	    return String.format("%06d", number);
	}
}
