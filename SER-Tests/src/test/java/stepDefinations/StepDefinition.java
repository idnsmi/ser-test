package stepDefinations;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

import java.io.IOException;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import resources.APIResources;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import junit.framework.Assert;
import pojo.propertyDemandRoot;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import resources.TestDataBuilder;
import resources.utils;

public class StepDefinition extends utils {

	static String resultoken;
	RequestSpecification request;
	ResponseSpecification responseSpec;
	TestDataBuilder data = new TestDataBuilder();
	Response res;
	
	String errorMessage = "request.body.events[0].eventId should be string, request.body.events[0].subEventId should be string, request.body.events[0].recurrenceId should be string, request.body.events[0].seriesId should be string, request.body.events[0].correlationId should be string, request.body.events[0].eventName should be string, request.body.events[0].description should be string, request.body.events[0].startDateTime should be object,string, request.body.events[0].endDateTime should be object,string, request.body.events[0].captureDate should be object,string, request.body.events[0].category should be string, request.body.events[0].category should be equal to one of the allowed values: SPORTS, HOLIDAY, ENTERTAINMENT, COMMUNITY, BUSINESS, request.body.events[0].subCategory should be string, request.body.events[0].subCategory should be equal to one of the allowed values: ARTS, CONCERTS, THEATER, DANCE, COMEDY, FAMILY_KIDS, FILM_MEDIA, TOURISM, DAYS_OUT, CLUBS, ATTRACTIONS, AMUSEMENT, VISUAL_ART, CALENDAR, HISTORIC, RELIGIOUS, FESTIVAL, NBA, NFL, NHL, MLB, SPORTS, MLS, TENNIS, AMERICAN_FOOTBALL, BASEBALL, SOCCER_FOOTBALL, ICE_HOCKEY, CRICKET, GOLF, AUTO_RACING, BOXING, WRESTLING, BASKETBALL, HOCKEY, CYCLING, TRADESHOWS_FAIRS_EXPO, CONFERENCES, WORKSHOPS, MEETUPS, SEMINAR_CLASSES, LECTURES_TALKS, BUSINESS_TECH, HEALTH_WELLNESS, ACADEMIC, FASHION, MARATHON, PROMOTIONAL, COMMUNITY, PARADE, POLITICAL_SOCIAL, GOVERNMENT, FOOD_FESTIVAL, CHARITY_CAUSES, COMMUNITY_PARTY, SPIRITUALITY, TRIATHLON, FOLKLORIC, HEALTH_WELLNESS, CELEBRATION, NATIONAL_HOLIDAY, BANK_HOLIDAY, PUBLIC_HOLIDAY, STATE_HOLIDAY, LOCAL_HOLIDAY, RELIGIOUS_HOLIDAY, GOVERNMENT_HOLIDAY, REGIONAL_HOLIDAY, RUGBY, VOLLEYBALL, request.body.events[0].eventStatus should be string, request.body.events[0].eventStatus should be equal to one of the allowed values: VALID, CANCELLED, POSTPONED, RESCHEDULED, UNABLE_TO_CONFIRM, request.body.events[0].estimatedAttendees should be number, request.body.events[0].capacity should be number, request.body.events[0].attendanceClass should be string, request.body.events[0].attendanceClass should be equal to one of the allowed values: A, B, C, D, E, F, request.body.events[0].sourceId should be string, request.body.events[0].location.city should be string, request.body.events[0].location.countrySubdivsion should be string, request.body.events[0].location.country should be string, request.body.events[0].location.region should be string, request.body.events[0].location.region should be equal to one of the allowed values: ANTARCTICA, SOUTH_AMERICA, OCEANIA, NORTH_AMERICA, ASIA, EUROPE, AFRICA, request.body.events[0].location.venue.venueId should be string, request.body.events[0].location.venue.venueName should be string, request.body.events[0].location.venue.address should be string, request.body.events[0].location.venue.address2 should be string, request.body.events[0].location.venue.postalCode should be string, request.body.events[0].location.venue.latitude should be number, request.body.events[0].location.venue.longitude should be number, request.body.metadata.sessionId should be string, request.body.metadata.pageSize should be integer, request.body.metadata.currentPage should be integer, request.body.metadata.numberOfPages should be integer, request.body.metadata.totalElements should be integer";
	String errorFieldMessage = "request.body.events[0] should have required property 'eventId', request.body.events[0] should have required property 'subEventId', request.body.events[0] should have required property 'eventName', request.body.events[0] should have required property 'startDateTime', request.body.events[0] should have required property 'endDateTime', request.body.events[0] should have required property 'category', request.body.events[0] should have required property 'subCategory', request.body.events[0].location should have required property 'country', request.body.events[0].location should have required property 'region', request.body.metadata should have required property 'sessionId', request.body.metadata should have required property 'pageSize', request.body.metadata should have required property 'currentPage', request.body.metadata should have required property 'numberOfPages', request.body.metadata should have required property 'totalElements'";
	String errorPDMessage = "request.body.propertyDemandIndicators[0].propertyId should be string, request.body.propertyDemandIndicators[0].ideasPropertyId should be string, request.body.propertyDemandIndicators[0].propertyName should be string, request.body.propertyDemandIndicators[0].proximityRange.number should be number, request.body.propertyDemandIndicators[0].proximityRange.unit should be string, request.body.propertyDemandIndicators[0].scores[0].demandScore should be number, request.body.propertyDemandIndicators[0].scores[0].forDate should be object,string, request.body.metadata.sessionId should be string, request.body.metadata.pageSize should be integer, request.body.metadata.currentPage should be integer, request.body.metadata.numberOfPages should be integer, request.body.metadata.totalElements should be integer";
	String errorFieldPDMessage = "request.body.metadata should have required property 'sessionId', request.body.metadata should have required property 'pageSize', request.body.metadata should have required property 'currentPage', request.body.metadata should have required property 'numberOfPages', request.body.metadata should have required property 'totalElements'";
	
	@Given("Add SER payload with valid token")
	public void add_ser_payload_with_valid_token() throws IOException {

		request = given()//.log().all()
				.spec(requestSpecification()).body(TestDataBuilder.addSERPayload());

	}
	
	@Given("Add SER payload with invalid token")
	public void add_ser_payload_with_invalid_token() throws IOException {
	    // Write code here that turns the phrase above into concrete actions
		request = given()//.log().all()
				.spec(invalidRequestSpecification()).body(TestDataBuilder.addSERPayload());
	}
	
	@Given("Add SER payload with invalid fields")
	public void add_ser_payload_with_invalid_fields() throws IOException {
		request = given()//.log().all()
				.spec(requestSpecification()).body(TestDataBuilder.addInvalidSERPayload());
	}
	
	@Given("Add SER payload without compulsory fields")
	public void add_ser_payload_without_compulsory_fields() throws IOException {
		request = given()//.log().all()
				.spec(requestSpecification()).body(TestDataBuilder.addNonCompulsoryFieldSERPayload());
	}
	
	@Given("Add Property Demand payload  with valid token")
	public void add_property_demand_payload_with_valid_token() throws IOException {
		request = given()//.log().all()
				.spec(requestSpecificationPropertyDemand()).body(TestDataBuilder.addPropertyDemandPayload());
	}
	
	@Given("Add Property Demand payload  with invalid token")
	public void add_property_demand_payload_with_invalid_token() throws IOException {
		request = given()//.log().all()
				.spec(invalidrequestSpecificationPropertyDemand()).body(TestDataBuilder.addPropertyDemandPayload());
	}

	@When("system calls SpecialEvents API with PUT http request")
	public void system_calls_special_events_api_with_put_http_request() {
		responseSpec = new ResponseSpecBuilder().expectStatusCode(200).expectContentType(ContentType.JSON).build();
		res = request.when().put("/special-events/v1");

	}
	
	@Given("Add Property Demand payload  with invalid fields")
	public void add_property_demand_payload_with_invalid_fields() throws IOException {
		request = given()//.log().all()
				.spec(requestSpecificationPropertyDemand()).body(TestDataBuilder.addInvalidPropertyDemandPayload());
	}
	
	@Given("Add Property Demand payload  without compulsory fields")
	public void add_property_demand_payload_without_compulsory_fields() throws IOException {
		request = given()//.log().all()
				.spec(requestSpecificationPropertyDemand()).body(TestDataBuilder.addNonCompulsoryPropertyDemandPayload());
	}
	
	@When("system calls PropertyDemands API with PUT http request")
	public void system_calls_property_demands_api_with_put_http_request() {
		responseSpec = new ResponseSpecBuilder().expectStatusCode(200).expectContentType(ContentType.JSON).build();
		res = request.when().put("/api/property-demands/v1");
	}

	@Then("the API call got success with status code {int}")
	public void the_api_call_got_success_with_status_code(Integer statusCode) {

		res.then()//.log().all()
		.assertThat().statusCode(statusCode);
	}

	@Then("{string} in response body")
	public void in_response_body(String responseText) {

		String responseBody = res.then()//.log().all()
				.extract().response().asString();
		//System.out.println(responseBody);

		assertEquals(responseBody, responseText);
	}
	
	@Then("Failure {string} text in response body")
	public void failure_text_in_response_body(String failureString) {
		res.then()//.log().all()
		.assertThat().body("message", equalTo(failureString));
	}
	
	@Then("Failure text in response body")
	public void failure_text_in_response_body() {
		res.then()//.log().all()
		.assertThat().body("message", equalTo(errorMessage));
	}
	
	@Then("Failure text of compulsory fields in response body")
	public void failure_text_of_compulsory_fields_in_response_body() {
		res.then()//.log().all()
		.assertThat().body("message", equalTo(errorFieldMessage));
	}
	
	@Then("Check if the Property Demand has got added")
	public void check_if_the_property_demand_has_got_added() throws IOException {
		
		
		request = given()//.log().all()
				.spec(requestSpecificationPropertyDemand()).body(TestDataBuilder.addPropertyDemandPayload());
		responseSpec = new ResponseSpecBuilder().expectStatusCode(200).expectContentType(ContentType.JSON).build();
		res = request.when().get("api/property-demands-outbound/v1/5db419a2-85f7-4a3c-9ca5-1e09a62dbdd5");
		
		res.then()//.log().all().assertThat()
		.statusCode(200)
		.body("propertyDemandIndicator.propertyId",equalTo("3101759"))
		.body("propertyDemandIndicator.ideasPropertyId",equalTo("5db419a2-85f7-4a3c-9ca5-1e09a62dbdd5"))
		.body("propertyDemandIndicator.propertyName",equalTo("Bahia Mar Ft. Lauderdale Beach- a DoubleTree by Hilton Hotel"))
		.body("propertyDemandIndicator.proximityRange.number",equalTo(1))
		.body("propertyDemandIndicator.proximityRange.unit",equalTo("meters"))
		.body("propertyDemandIndicator.scores[0].demandScore",equalTo(59.6F))
		.body("propertyDemandIndicator.scores[0].forDate",equalTo("2022-01-06"))
		.body("propertyDemandIndicator.innCode",equalTo("FLLBM"))
		.body("propertyDemandIndicator.vendor",equalTo("TEST"))
		.body("count",equalTo(1));		
		
		//String prpdmdResponse = res.asString();
		
		propertyDemandRoot respo = res.getBody().as(propertyDemandRoot.class); 
		//System.out.println(respo.getCount());
		//System.out.println(respo.getPropertyDemandIndicator().getPropertyName());
		
		assertEquals(1, respo.getCount());
		assertEquals("Bahia Mar Ft. Lauderdale Beach- a DoubleTree by Hilton Hotel", respo.getPropertyDemandIndicator().getPropertyName());

	}

	@Then("Failure text for Property Demand in response body")
	public void failure_text_for_property_demand_in_response_body() {
		res.then()//.log().all()
		.assertThat().body("message", equalTo(errorPDMessage));
	}

}
